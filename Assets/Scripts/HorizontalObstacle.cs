﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HorizontalObstacle : MonoBehaviour
{
    [SerializeField]
    private float speed;

    [SerializeField]
    private float startDelay;

    [SerializeField]
    private float timer = 2f;

    private int x = -1;

    void Update()
    {
        // Star Delay, 
		if (startDelay > 0)
		{
            startDelay -= Time.deltaTime;
            return;
        }

        //The timer determines when the obstacle will turn.
        timer -= Time.deltaTime;

        if(timer < 0)
		{
            timer = 5f;

            //reverses on the x-axis
            x *= -1;
		}

        transform.Translate(new Vector3(x, 0f, 0f) * speed * Time.deltaTime);


    
    }
}
