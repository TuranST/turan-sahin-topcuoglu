﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Paint : MonoBehaviour
{
    public GameObject brush;

	Vector3 lastPos;

	List<Vector3> spawnPoints;

	private int i = 0;

	private void Update()
	{
		

		if (Input.GetMouseButton(0))
		{
			var Ray = Camera.main.ScreenPointToRay(Input.mousePosition);

			RaycastHit hit;

			if(Physics.Raycast(Ray, out hit))
			{
				if (lastPos != hit.point)
				{

					Instantiate(brush, hit.point, Quaternion.identity,transform);

					lastPos = hit.point;
					

				}
			}
		}
	}
}
