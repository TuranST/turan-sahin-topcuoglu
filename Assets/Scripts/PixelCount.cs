﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class PixelCount : MonoBehaviour
{
    [SerializeField] private RenderTexture renderTexture = null;

    [SerializeField] private int numParts = 1;

    [SerializeField] private Image percentSprite;

    [SerializeField] private Text percentText;

    private int texWidth, texHeight;
    private int numPixels;
    private int numPixelsInPart;

    private Texture2D readTexture;
    private Color32[] colors;

    private int currentPart = 0;

    private int[] numTransparentPixelsInParts;

    private int numTransparentPixels;

    private float percent = 0;

    private bool isGameEnd = false;

    private void Start()
    {
        numTransparentPixelsInParts = new int[numParts];

        texWidth = renderTexture.width;
        texHeight = renderTexture.height;

        numPixels = texWidth * texHeight;
        numPixelsInPart = numPixels / numParts;

        readTexture = new Texture2D(texWidth, texHeight);
    }

    private void Update()
    {
        UpdateColors();

        UpdateCounter();
       // Debug.Log("Num transparent pixels: " + numTransparentPixels);

        percent = numTransparentPixels / 921600f * 100f;

        percentSprite.fillAmount = percent / 100f;

        percentText.text = "%" + percent.ToString();

        if (percent == 100 && !isGameEnd)
		{
            FindObjectOfType<FinishLine>().EndOfGame();
            isGameEnd = true;
        }
           
        //Debug.Log(percent);
    }

    private void UpdateColors()
    {
        UpdateReadTexture();
        colors = readTexture.GetPixels32();
    }

    private void UpdateReadTexture()
    {
        RenderTexture.active = renderTexture;
        readTexture.ReadPixels(new Rect(0, 0, texWidth, texHeight), 0, 0);
        readTexture.Apply();
        RenderTexture.active = null;
    }

    private void UpdateCounter()
    {
        numTransparentPixels -= numTransparentPixelsInParts[currentPart];
        numTransparentPixelsInParts[currentPart] = 0;

        CountTransparentPixels();

        numTransparentPixels += numTransparentPixelsInParts[currentPart];
    }

    private void CountTransparentPixels()
    {
        int startPixelIndex = currentPart * numPixelsInPart;
        int endPixelIndex = (currentPart + 1) * numPixelsInPart;

        for (int i = startPixelIndex; i < endPixelIndex; ++i)
        {
            if (colors[i].r > 250) { numTransparentPixelsInParts[currentPart]++; }
        }
    }
}
