﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationPlatform : MonoBehaviour
{
	// for scale error's fix.
	public GameObject parentObj;

	public void OnTriggerStay(Collider other)
	{
		if (other.CompareTag("player"))
		{
			other.gameObject.transform.parent = parentObj.transform;
		}

	}

	private void OnTriggerExit(Collider other)
	{
		if (other.CompareTag("player"))
		{
			other.gameObject.transform.parent = null;
		}
	}
}
