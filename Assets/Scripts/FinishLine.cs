﻿using UnityEngine;
using UnityEngine.UI;

public class FinishLine : MonoBehaviour
{
	[SerializeField]
	GameObject paintWall;

	[SerializeField]
	GameObject percentUI, gameOverImage;

	private void OnTriggerEnter(Collider other)
	{
		if (other.CompareTag("player"))
		{
			Finish(other);
		}
	}

	void Finish(Collider _other)
	{
		_other.GetComponent<PlayerController>().enabled = false;
		_other.GetComponent<Animator>().SetBool("speed", false);
		_other.gameObject.transform.position = new Vector3(0f, 1f, 226f);

		//Camera will move in head
		FindObjectOfType<CameraFollow>().gameFinish = true;

		//Paint UI activate
		percentUI.SetActive(true);

		//Paint code will activate
		FindObjectOfType<Paint>().enabled = true;

		//Paint Wall activate
		paintWall.SetActive(true);

	}

	public void EndOfGame()
	{
		// Game is Done
		gameOverImage.SetActive(true);
		Debug.Log("GAME OVER");
	}

}
