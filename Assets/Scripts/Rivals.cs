﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rivals : MonoBehaviour
{
	public LayerMask obs;
	public LayerMask groundMask;

	public Transform groundCheck;
	
	public float gorundDistance = 0.4f;

	[SerializeField]
	private CharacterController controller;

	Animator anim;

	Quaternion rotation;

	Vector3 velocity;

	private readonly float gravity = -9.81f;

	bool isGrounded;

	Vector3 middleOfPlayer = new Vector3(0f, 1f, 0f);
	Vector3 rightCheck = new Vector3(3f, 0, 10f);
	Vector3 leftCheck = new Vector3(-3f, 0, 10f);
	Vector3 midCheck = new Vector3(0, 0, 10f);

	private readonly float raySize = 8f;

	float delay;
	int random;


	void Start()
	{
		CharacterController characterController = GetComponent<CharacterController>();
		controller = characterController;

		anim = gameObject.GetComponent<Animator>();
		//anim.SetBool("speed", false);
	}

	private void Update()
	{
		if (delay > 0)
		{
			delay -= Time.deltaTime;
			return;
		}
		IsGround();
		Debug.DrawRay(transform.position + middleOfPlayer, new Vector3(0f, 0f, 10f), Color.red);
		Debug.DrawRay(transform.position + middleOfPlayer, new Vector3(5f, 0f, 10f), Color.red);
		Debug.DrawRay(transform.position + middleOfPlayer, new Vector3(-5f, 0f, 10f), Color.red);

		velocity.z = 1f;
		velocity.x = 0f;

		random = Random.Range(0, 2);
		if (Physics.Raycast(transform.position + middleOfPlayer, midCheck, raySize, obs))
		{
			if (!Physics.Raycast(transform.position + middleOfPlayer, rightCheck, raySize, obs) && transform.position.x < 13f && random == 0)
			{
				anim.SetBool("speed", true);
				velocity.x = .5f;
				velocity.z = 1f;
				rotation = Quaternion.Euler(0, 15f, 0);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
			}
			else
			if (!Physics.Raycast(transform.position + middleOfPlayer, leftCheck, raySize, obs) && transform.position.x > -13f && random == 1)
			{
				anim.SetBool("speed", true);
				velocity.x = -.5f;
				velocity.z = 1f;
				rotation = Quaternion.Euler(0, -15f, 0);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
			}
			else
			{
				anim.SetBool("speed", false);
				velocity.x = 0;
				velocity.z = 0;
				rotation = Quaternion.Euler(0, 0, 0);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
			}
		}

		if (Physics.Raycast(transform.position + middleOfPlayer, leftCheck, raySize, obs))
		{
			if (!Physics.Raycast(transform.position + middleOfPlayer, rightCheck, raySize, obs) && transform.position.x < 13f)
			{
				anim.SetBool("speed", true);
				velocity.x = .5f;
				velocity.z = 1f;
				rotation = Quaternion.Euler(0, 15f, 0);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
			}
			else
			if (!Physics.Raycast(transform.position + middleOfPlayer, midCheck, raySize, obs))
			{
				anim.SetBool("speed", true);
				velocity.x = -.5f;
				velocity.z = 1f;
				rotation = Quaternion.Euler(0, -15f, 0);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
			}
			else
			{
				anim.SetBool("speed", false);
				velocity.x = 0;
				velocity.z = 0;
				rotation = Quaternion.Euler(0, 0, 0);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
			}
		}

		if (Physics.Raycast(transform.position + middleOfPlayer, rightCheck, raySize, obs))
		{
			if (!Physics.Raycast(transform.position + middleOfPlayer, midCheck, raySize, obs))
			{
				anim.SetBool("speed", true);
				velocity.x = .5f;
				velocity.z = 1f;
				rotation = Quaternion.Euler(0, 15f, 0);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
			}
			else
			if (!Physics.Raycast(transform.position + middleOfPlayer, leftCheck, raySize, obs) && transform.position.x > -13f)
			{
				anim.SetBool("speed", true);
				velocity.x = -.5f;
				velocity.z = 1f;
				rotation = Quaternion.Euler(0, -15f, 0);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
			}
			else
			{
				anim.SetBool("speed", false);
				velocity.x = 0;
				velocity.z = 0;
				rotation = Quaternion.Euler(0, 0, 0);
				transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
			}
		}


		rotation = Quaternion.Euler(0, 0, 0);
		transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);

		// When touched the screen Player will run.


		//Activate run animation.
		//anim.SetBool("speed", true);

		velocity.y += gravity * Time.deltaTime;
		controller.Move(velocity * Time.deltaTime * 7f);
	}
	private void OnTriggerStay(Collider other)
	{
		//If player hits any obstacle, Player will star over.
		if (other.CompareTag("obstacle"))
		{
			float x = Random.Range(-10f, 10);
			float z = Random.Range(-2f, 0.4f);
			transform.position = new Vector3(x, 1f, z);
			velocity.z = 0;
			delay = 0.3f;
			transform.rotation = Quaternion.Euler(0f, 0f, 0f);
		}
	}

	void IsGround()
	{
		isGrounded = Physics.CheckSphere(groundCheck.position, gorundDistance, groundMask);

		if (isGrounded && velocity.y < 0)
		{
			velocity.y = 0f;
		}
	}
}
