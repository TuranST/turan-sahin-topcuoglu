﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rotatorScript : MonoBehaviour
{
	[SerializeField]
	private float rotationSpeed;

	[SerializeField]
	private Vector3 axis;

	private void FixedUpdate()
	{
		//Turnin on y-axis
		transform.Rotate(axis * rotationSpeed);

	}
}
