﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
	[SerializeField]
	Transform target;

	[SerializeField]
	float smoothSpeed = 0.125f;

	public Vector3 offset;
	public bool gameFinish = false;

	private void FixedUpdate()
	{
		Vector3 desiredPosition = target.position + offset;
		Vector3 smoothPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
		transform.position = smoothPosition;

		if (gameFinish)
			CamRotation();
		

		//transform.LookAt(target);
	}

	public void CamRotation()
	{
		offset = new Vector3(0f, 2f, 0f);
		transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0f, 0f, 0f), smoothSpeed);
	}
}
