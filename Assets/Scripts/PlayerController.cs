﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	public Transform groundCheck;
	public float gorundDistance = 0.4f;
	public LayerMask groundMask;
    public bool isHit = false;
    public bool forced = false;
    public float timer = 1f;

    [SerializeField]
	private CharacterController controller;

    [SerializeField]
	private float speed;

	private Touch touch;
	private float speedModifier;
	private Animator anim;
	private Quaternion rotation;
	private Vector3 velocity;
	private readonly float gravity = -9.81f;

    bool isGrounded;

    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();

        speedModifier = 0.02f;
		CharacterController characterController = GetComponent<CharacterController>();
		controller = characterController;

        anim = gameObject.GetComponent<Animator>();
        anim.SetBool("speed", false);
    }

    // Update is called once per frame
    void Update()
    {

		if (forced && timer >= 0)
		{
            controller.Move(new Vector3(0f, 0f, -1f) * (speed + 4) * Time.deltaTime);
            timer -= Time.deltaTime;
            return;
		}

        isGrounded = Physics.CheckSphere(groundCheck.position, gorundDistance, groundMask);

        if(isGrounded && velocity.y < 0)
		{
            velocity.y = 0f;
		}
		

       // controller.Move(Vector3.forward * speed * Time.deltaTime);

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime * speed);


        if (Input.touchCount > 0 || Input.GetMouseButton(0))
            MovePlayer();
        else
        {
            //Activate idle Animation.
            anim.SetBool("speed", false);

            rotation = Quaternion.Euler(0, 0, 0);
            transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);

            //Set 0 player velocity.
            velocity.z = 0f;
        }



    }

    void MovePlayer()
    {

        // When touched the screen Player will run.
        velocity.z = 1f;

        //Activate run animation.
        anim.SetBool("speed", true);

        touch = Input.GetTouch(0);

        //Player rotate where he move and if touch move will 0, player lookAt forward
        {
            if (touch.phase == TouchPhase.Moved)
            {
                transform.position = new Vector3(
                    transform.position.x + touch.deltaPosition.x * speedModifier,
                    transform.position.y,
                    transform.position.z);

                //Rotate Player  position
                if (touch.deltaPosition.x > 0)
                {
                    rotation = Quaternion.Euler(0, 15f, 0);
                    transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
                }

                if (touch.deltaPosition.x < 0)
                {
                    rotation = Quaternion.Euler(0, -15f, 0);
                    transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
                }
            }
            else
            {
                rotation = Quaternion.Euler(0, 0, 0);
                transform.rotation = Quaternion.Lerp(transform.rotation, rotation, 10f * Time.deltaTime);
            }
        }


    }


    private void OnTriggerStay(Collider other)
    {
        //If player hits any obstacle, Player will star over.
        if (other.CompareTag("obstacle"))
        {
            transform.position = new Vector3(0f, 1f, 0f);
        }
    }

    public void AddForcePlayer()
	{
        forced = true;
        timer = 1f;
	}

}
