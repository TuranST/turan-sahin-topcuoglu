<Q                         DIRLIGHTMAP_COMBINED   _ADDITIONAL_LIGHTS     _MAIN_LIGHT_SHADOWS    _MAIN_LIGHT_SHADOWS_CASCADE    _SHADOWS_SOFT       x  #ifdef VERTEX
#version 300 es

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 _TimeParameters;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
uniform 	vec4 _CascadeShadowSplitSpheres0;
uniform 	vec4 _CascadeShadowSplitSpheres1;
uniform 	vec4 _CascadeShadowSplitSpheres2;
uniform 	vec4 _CascadeShadowSplitSpheres3;
uniform 	vec4 _CascadeShadowSplitSphereRadii;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 Color_9DAE10ED;
	UNITY_UNIFORM float Vector1_5726EE9A;
	UNITY_UNIFORM float Vector1_348DD60F;
	UNITY_UNIFORM float Vector1_DE6E0ACB;
	UNITY_UNIFORM vec4 Color_45BF8EF3;
	UNITY_UNIFORM float Vector1_65135B8E;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
in highp vec3 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec4 in_TANGENT0;
in highp vec4 in_TEXCOORD0;
out highp vec3 vs_TEXCOORD0;
out highp vec3 vs_TEXCOORD1;
out highp vec4 vs_TEXCOORD2;
out highp vec4 vs_TEXCOORD3;
out highp vec3 vs_TEXCOORD4;
out highp vec2 vs_TEXCOORD5;
out highp vec3 vs_TEXCOORD6;
out highp vec4 vs_TEXCOORD7;
out highp vec4 vs_TEXCOORD8;
vec4 u_xlat0;
vec4 u_xlat1;
mediump vec4 u_xlat16_1;
bvec4 u_xlatb1;
vec4 u_xlat2;
mediump vec4 u_xlat16_2;
bvec4 u_xlatb2;
vec4 u_xlat3;
bvec4 u_xlatb3;
vec4 u_xlat4;
bvec4 u_xlatb4;
mediump vec3 u_xlat16_5;
mediump vec3 u_xlat16_6;
vec2 u_xlat7;
vec2 u_xlat8;
bool u_xlatb8;
vec2 u_xlat14;
bool u_xlatb14;
vec2 u_xlat15;
bool u_xlatb15;
vec2 u_xlat16;
float u_xlat21;
int u_xlati21;
uint u_xlatu21;
bool u_xlatb21;
float u_xlat22;
bool u_xlatb22;
void main()
{
    u_xlat0.xy = _TimeParameters.xx * vec2(Vector1_65135B8E) + in_TEXCOORD0.xy;
    u_xlat0.xy = u_xlat0.xy + u_xlat0.xy;
    u_xlat14.xy = floor(u_xlat0.xy);
    u_xlat0.xy = fract(u_xlat0.xy);
    u_xlat1.xy = u_xlat14.xy + vec2(1.0, 1.0);
    u_xlat2 = u_xlat1.xyxy * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb2 = greaterThanEqual(u_xlat2, (-u_xlat2.zwzw));
    u_xlat2.x = (u_xlatb2.x) ? float(289.0) : float(-289.0);
    u_xlat2.y = (u_xlatb2.y) ? float(289.0) : float(-289.0);
    u_xlat2.z = (u_xlatb2.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat2.w = (u_xlatb2.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat1.xy = u_xlat1.xy * u_xlat2.zw;
    u_xlat1.xy = fract(u_xlat1.xy);
    u_xlat1.xy = u_xlat1.xy * u_xlat2.xy;
    u_xlat15.x = u_xlat1.x * 34.0 + 1.0;
    u_xlat1.x = u_xlat1.x * u_xlat15.x;
    u_xlat15.x = u_xlat1.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb15 = !!(u_xlat15.x>=(-u_xlat15.x));
#else
    u_xlatb15 = u_xlat15.x>=(-u_xlat15.x);
#endif
    u_xlat15.xy = (bool(u_xlatb15)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat1.x = u_xlat15.y * u_xlat1.x;
    u_xlat1.x = fract(u_xlat1.x);
    u_xlat1.x = u_xlat15.x * u_xlat1.x + u_xlat1.y;
    u_xlat8.x = u_xlat1.x * 34.0 + 1.0;
    u_xlat1.x = u_xlat1.x * u_xlat8.x;
    u_xlat8.x = u_xlat1.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb8 = !!(u_xlat8.x>=(-u_xlat8.x));
#else
    u_xlatb8 = u_xlat8.x>=(-u_xlat8.x);
#endif
    u_xlat8.xy = (bool(u_xlatb8)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat1.x = u_xlat8.y * u_xlat1.x;
    u_xlat1.x = fract(u_xlat1.x);
    u_xlat1.x = u_xlat1.x * u_xlat8.x;
    u_xlat1.x = u_xlat1.x * 0.024390243;
    u_xlat1.x = fract(u_xlat1.x);
    u_xlat1.xy = u_xlat1.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat8.x = floor(u_xlat1.y);
    u_xlat2.x = (-u_xlat8.x) + u_xlat1.x;
    u_xlat2.y = abs(u_xlat1.x) + -0.5;
    u_xlat1.x = dot(u_xlat2.xy, u_xlat2.xy);
    u_xlat1.x = inversesqrt(u_xlat1.x);
    u_xlat1.xy = u_xlat1.xx * u_xlat2.xy;
    u_xlat15.xy = u_xlat0.xy + vec2(-1.0, -1.0);
    u_xlat1.x = dot(u_xlat1.xy, u_xlat15.xy);
    u_xlat2 = u_xlat14.xyxy + vec4(0.0, 1.0, 1.0, 0.0);
    u_xlat3 = u_xlat2 * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb3 = greaterThanEqual(u_xlat3, (-u_xlat3));
    u_xlat4.x = (u_xlatb3.z) ? float(289.0) : float(-289.0);
    u_xlat4.y = (u_xlatb3.w) ? float(289.0) : float(-289.0);
    u_xlat4.z = (u_xlatb3.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat4.w = (u_xlatb3.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat3.x = (u_xlatb3.x) ? float(289.0) : float(-289.0);
    u_xlat3.y = (u_xlatb3.y) ? float(289.0) : float(-289.0);
    u_xlat3.z = (u_xlatb3.x) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat3.w = (u_xlatb3.y) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat8.xy = u_xlat2.zw * u_xlat4.zw;
    u_xlat2.xy = u_xlat2.xy * u_xlat3.zw;
    u_xlat2.xy = fract(u_xlat2.xy);
    u_xlat2.xy = u_xlat2.xy * u_xlat3.xy;
    u_xlat8.xy = fract(u_xlat8.xy);
    u_xlat8.xy = u_xlat8.xy * u_xlat4.xy;
    u_xlat22 = u_xlat8.x * 34.0 + 1.0;
    u_xlat8.x = u_xlat8.x * u_xlat22;
    u_xlat22 = u_xlat8.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb22 = !!(u_xlat22>=(-u_xlat22));
#else
    u_xlatb22 = u_xlat22>=(-u_xlat22);
#endif
    u_xlat16.xy = (bool(u_xlatb22)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat8.x = u_xlat8.x * u_xlat16.y;
    u_xlat8.x = fract(u_xlat8.x);
    u_xlat8.x = u_xlat16.x * u_xlat8.x + u_xlat8.y;
    u_xlat15.x = u_xlat8.x * 34.0 + 1.0;
    u_xlat8.x = u_xlat8.x * u_xlat15.x;
    u_xlat15.x = u_xlat8.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb15 = !!(u_xlat15.x>=(-u_xlat15.x));
#else
    u_xlatb15 = u_xlat15.x>=(-u_xlat15.x);
#endif
    u_xlat15.xy = (bool(u_xlatb15)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat8.x = u_xlat15.y * u_xlat8.x;
    u_xlat8.x = fract(u_xlat8.x);
    u_xlat8.x = u_xlat8.x * u_xlat15.x;
    u_xlat8.x = u_xlat8.x * 0.024390243;
    u_xlat8.x = fract(u_xlat8.x);
    u_xlat8.xy = u_xlat8.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat15.x = floor(u_xlat8.y);
    u_xlat3.x = (-u_xlat15.x) + u_xlat8.x;
    u_xlat3.y = abs(u_xlat8.x) + -0.5;
    u_xlat8.x = dot(u_xlat3.xy, u_xlat3.xy);
    u_xlat8.x = inversesqrt(u_xlat8.x);
    u_xlat8.xy = u_xlat8.xx * u_xlat3.xy;
    u_xlat3 = u_xlat0.xyxy + vec4(-0.0, -1.0, -1.0, -0.0);
    u_xlat8.x = dot(u_xlat8.xy, u_xlat3.zw);
    u_xlat1.x = (-u_xlat8.x) + u_xlat1.x;
    u_xlat15.xy = u_xlat0.xy * u_xlat0.xy;
    u_xlat15.xy = u_xlat0.xy * u_xlat15.xy;
    u_xlat16.xy = u_xlat0.xy * vec2(6.0, 6.0) + vec2(-15.0, -15.0);
    u_xlat16.xy = u_xlat0.xy * u_xlat16.xy + vec2(10.0, 10.0);
    u_xlat15.xy = u_xlat15.xy * u_xlat16.xy;
    u_xlat1.x = u_xlat15.y * u_xlat1.x + u_xlat8.x;
    u_xlat4 = u_xlat14.xyxy * vec4(289.0, 289.0, 289.0, 289.0);
    u_xlatb4 = greaterThanEqual(u_xlat4, (-u_xlat4.zwzw));
    u_xlat4.x = (u_xlatb4.x) ? float(289.0) : float(-289.0);
    u_xlat4.y = (u_xlatb4.y) ? float(289.0) : float(-289.0);
    u_xlat4.z = (u_xlatb4.z) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat4.w = (u_xlatb4.w) ? float(0.00346020772) : float(-0.00346020772);
    u_xlat14.xy = u_xlat14.xy * u_xlat4.zw;
    u_xlat14.xy = fract(u_xlat14.xy);
    u_xlat14.xy = u_xlat14.xy * u_xlat4.xy;
    u_xlat8.x = u_xlat14.x * 34.0 + 1.0;
    u_xlat14.x = u_xlat14.x * u_xlat8.x;
    u_xlat8.x = u_xlat14.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb8 = !!(u_xlat8.x>=(-u_xlat8.x));
#else
    u_xlatb8 = u_xlat8.x>=(-u_xlat8.x);
#endif
    u_xlat16.xy = (bool(u_xlatb8)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat14.x = u_xlat14.x * u_xlat16.y;
    u_xlat14.x = fract(u_xlat14.x);
    u_xlat14.x = u_xlat16.x * u_xlat14.x + u_xlat14.y;
    u_xlat21 = u_xlat14.x * 34.0 + 1.0;
    u_xlat14.x = u_xlat14.x * u_xlat21;
    u_xlat21 = u_xlat14.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb21 = !!(u_xlat21>=(-u_xlat21));
#else
    u_xlatb21 = u_xlat21>=(-u_xlat21);
#endif
    u_xlat16.xy = (bool(u_xlatb21)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat14.x = u_xlat14.x * u_xlat16.y;
    u_xlat14.x = fract(u_xlat14.x);
    u_xlat14.x = u_xlat14.x * u_xlat16.x;
    u_xlat14.x = u_xlat14.x * 0.024390243;
    u_xlat14.x = fract(u_xlat14.x);
    u_xlat14.xy = u_xlat14.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat21 = floor(u_xlat14.y);
    u_xlat4.x = (-u_xlat21) + u_xlat14.x;
    u_xlat4.y = abs(u_xlat14.x) + -0.5;
    u_xlat14.x = dot(u_xlat4.xy, u_xlat4.xy);
    u_xlat14.x = inversesqrt(u_xlat14.x);
    u_xlat14.xy = u_xlat14.xx * u_xlat4.xy;
    u_xlat0.x = dot(u_xlat14.xy, u_xlat0.xy);
    u_xlat7.x = u_xlat2.x * 34.0 + 1.0;
    u_xlat7.x = u_xlat2.x * u_xlat7.x;
    u_xlat14.x = u_xlat7.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb14 = !!(u_xlat14.x>=(-u_xlat14.x));
#else
    u_xlatb14 = u_xlat14.x>=(-u_xlat14.x);
#endif
    u_xlat14.xy = (bool(u_xlatb14)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat7.x = u_xlat14.y * u_xlat7.x;
    u_xlat7.x = fract(u_xlat7.x);
    u_xlat7.x = u_xlat14.x * u_xlat7.x + u_xlat2.y;
    u_xlat14.x = u_xlat7.x * 34.0 + 1.0;
    u_xlat7.x = u_xlat7.x * u_xlat14.x;
    u_xlat14.x = u_xlat7.x * 289.0;
#ifdef UNITY_ADRENO_ES3
    u_xlatb14 = !!(u_xlat14.x>=(-u_xlat14.x));
#else
    u_xlatb14 = u_xlat14.x>=(-u_xlat14.x);
#endif
    u_xlat14.xy = (bool(u_xlatb14)) ? vec2(289.0, 0.00346020772) : vec2(-289.0, -0.00346020772);
    u_xlat7.x = u_xlat14.y * u_xlat7.x;
    u_xlat7.x = fract(u_xlat7.x);
    u_xlat7.x = u_xlat7.x * u_xlat14.x;
    u_xlat7.x = u_xlat7.x * 0.024390243;
    u_xlat7.x = fract(u_xlat7.x);
    u_xlat7.xy = u_xlat7.xx * vec2(2.0, 2.0) + vec2(-1.0, -0.5);
    u_xlat14.x = floor(u_xlat7.y);
    u_xlat2.x = (-u_xlat14.x) + u_xlat7.x;
    u_xlat2.y = abs(u_xlat7.x) + -0.5;
    u_xlat7.x = dot(u_xlat2.xy, u_xlat2.xy);
    u_xlat7.x = inversesqrt(u_xlat7.x);
    u_xlat7.xy = u_xlat7.xx * u_xlat2.xy;
    u_xlat7.x = dot(u_xlat7.xy, u_xlat3.xy);
    u_xlat7.x = (-u_xlat0.x) + u_xlat7.x;
    u_xlat0.x = u_xlat15.y * u_xlat7.x + u_xlat0.x;
    u_xlat7.x = (-u_xlat0.x) + u_xlat1.x;
    u_xlat0.x = u_xlat15.x * u_xlat7.x + u_xlat0.x;
    u_xlat0.x = u_xlat0.x + 0.5;
    u_xlat0.xyz = in_NORMAL0.xyz * u_xlat0.xxx + in_POSITION0.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].xyz;
    u_xlat0.xyw = hlslcc_mtx4x4unity_ObjectToWorld[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].xyz * u_xlat0.zzz + u_xlat0.xyw;
    u_xlat0.xyz = u_xlat0.xyz + hlslcc_mtx4x4unity_ObjectToWorld[3].xyz;
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat0.zzzz + u_xlat1;
    gl_Position = u_xlat1 + hlslcc_mtx4x4unity_MatrixVP[3];
    vs_TEXCOORD0.xyz = u_xlat0.xyz;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat21 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat21 = max(u_xlat21, 1.17549435e-38);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat1.xyz;
    vs_TEXCOORD1.xyz = u_xlat1.xyz;
    u_xlat2.x = hlslcc_mtx4x4unity_ObjectToWorld[0].x;
    u_xlat2.y = hlslcc_mtx4x4unity_ObjectToWorld[1].x;
    u_xlat2.z = hlslcc_mtx4x4unity_ObjectToWorld[2].x;
    u_xlat16_5.x = dot(u_xlat2.xyz, in_TANGENT0.xyz);
    u_xlat2.x = hlslcc_mtx4x4unity_ObjectToWorld[0].y;
    u_xlat2.y = hlslcc_mtx4x4unity_ObjectToWorld[1].y;
    u_xlat2.z = hlslcc_mtx4x4unity_ObjectToWorld[2].y;
    u_xlat16_5.y = dot(u_xlat2.xyz, in_TANGENT0.xyz);
    u_xlat2.x = hlslcc_mtx4x4unity_ObjectToWorld[0].z;
    u_xlat2.y = hlslcc_mtx4x4unity_ObjectToWorld[1].z;
    u_xlat2.z = hlslcc_mtx4x4unity_ObjectToWorld[2].z;
    u_xlat16_5.z = dot(u_xlat2.xyz, in_TANGENT0.xyz);
    u_xlat21 = dot(u_xlat16_5.xyz, u_xlat16_5.xyz);
    u_xlat21 = max(u_xlat21, 1.17549435e-38);
    u_xlat21 = inversesqrt(u_xlat21);
    vs_TEXCOORD2.xyz = vec3(u_xlat21) * u_xlat16_5.xyz;
    vs_TEXCOORD2.w = in_TANGENT0.w;
    vs_TEXCOORD3 = in_TEXCOORD0;
    vs_TEXCOORD4.xyz = (-u_xlat0.xyz) + _WorldSpaceCameraPos.xyz;
    vs_TEXCOORD5.xy = vec2(0.0, 0.0);
    u_xlat16_5.x = u_xlat1.y * u_xlat1.y;
    u_xlat16_5.x = u_xlat1.x * u_xlat1.x + (-u_xlat16_5.x);
    u_xlat16_2 = u_xlat1.yzzx * u_xlat1.xyzz;
    u_xlat16_6.x = dot(unity_SHBr, u_xlat16_2);
    u_xlat16_6.y = dot(unity_SHBg, u_xlat16_2);
    u_xlat16_6.z = dot(unity_SHBb, u_xlat16_2);
    u_xlat16_5.xyz = unity_SHC.xyz * u_xlat16_5.xxx + u_xlat16_6.xyz;
    u_xlat1.w = 1.0;
    u_xlat16_6.x = dot(unity_SHAr, u_xlat1);
    u_xlat16_6.y = dot(unity_SHAg, u_xlat1);
    u_xlat16_6.z = dot(unity_SHAb, u_xlat1);
    u_xlat16_5.xyz = u_xlat16_5.xyz + u_xlat16_6.xyz;
    u_xlat16_5.xyz = max(u_xlat16_5.xyz, vec3(0.0, 0.0, 0.0));
    vs_TEXCOORD6.xyz = u_xlat16_5.xyz;
    vs_TEXCOORD7 = vec4(0.0, 0.0, 0.0, 0.0);
    u_xlat1.xyz = u_xlat0.xyz + (-_CascadeShadowSplitSpheres0.xyz);
    u_xlat1.x = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat3.xyz = u_xlat0.xyz + (-_CascadeShadowSplitSpheres1.xyz);
    u_xlat1.y = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat3.xyz = u_xlat0.xyz + (-_CascadeShadowSplitSpheres2.xyz);
    u_xlat1.z = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat3.xyz = u_xlat0.xyz + (-_CascadeShadowSplitSpheres3.xyz);
    u_xlat1.w = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlatb1 = lessThan(u_xlat1, _CascadeShadowSplitSphereRadii);
    u_xlat16_5.x = (u_xlatb1.x) ? float(-1.0) : float(-0.0);
    u_xlat16_5.y = (u_xlatb1.y) ? float(-1.0) : float(-0.0);
    u_xlat16_5.z = (u_xlatb1.z) ? float(-1.0) : float(-0.0);
    u_xlat16_1.x = (u_xlatb1.x) ? float(1.0) : float(0.0);
    u_xlat16_1.y = (u_xlatb1.y) ? float(1.0) : float(0.0);
    u_xlat16_1.z = (u_xlatb1.z) ? float(1.0) : float(0.0);
    u_xlat16_1.w = (u_xlatb1.w) ? float(1.0) : float(0.0);
    u_xlat16_5.xyz = u_xlat16_5.xyz + u_xlat16_1.yzw;
    u_xlat16_1.yzw = max(u_xlat16_5.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_5.x = dot(u_xlat16_1, vec4(4.0, 3.0, 2.0, 1.0));
    u_xlat16_5.x = (-u_xlat16_5.x) + 4.0;
    u_xlatu21 = uint(u_xlat16_5.x);
    u_xlati21 = int(int(u_xlatu21) << 2);
    u_xlat1 = u_xlat0.yyyy * hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati21 + 1)];
    u_xlat1 = hlslcc_mtx4x4_MainLightWorldToShadow[u_xlati21] * u_xlat0.xxxx + u_xlat1;
    u_xlat1 = hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati21 + 2)] * u_xlat0.zzzz + u_xlat1;
    vs_TEXCOORD8 = u_xlat1 + hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati21 + 3)];
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es
#ifdef GL_EXT_shader_texture_lod
#extension GL_EXT_shader_texture_lod : enable
#endif

precision highp float;
precision highp int;
#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
vec4 ImmCB_0_0_0[4];
uniform 	vec4 _MainLightPosition;
uniform 	mediump vec4 _MainLightColor;
uniform 	mediump vec4 _AdditionalLightsCount;
uniform 	vec4 _AdditionalLightsPosition[32];
uniform 	mediump vec4 _AdditionalLightsColor[32];
uniform 	mediump vec4 _AdditionalLightsAttenuation[32];
uniform 	mediump vec4 _AdditionalLightsSpotDir[32];
uniform 	vec4 _TimeParameters;
uniform 	vec4 hlslcc_mtx4x4_MainLightWorldToShadow[20];
uniform 	vec4 _CascadeShadowSplitSpheres0;
uniform 	vec4 _CascadeShadowSplitSpheres1;
uniform 	vec4 _CascadeShadowSplitSpheres2;
uniform 	vec4 _CascadeShadowSplitSpheres3;
uniform 	vec4 _CascadeShadowSplitSphereRadii;
uniform 	mediump vec4 _MainLightShadowOffset0;
uniform 	mediump vec4 _MainLightShadowOffset1;
uniform 	mediump vec4 _MainLightShadowOffset2;
uniform 	mediump vec4 _MainLightShadowOffset3;
uniform 	mediump vec4 _MainLightShadowParams;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(0) uniform UnityPerDraw {
#endif
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
	UNITY_UNIFORM vec4 hlslcc_mtx4x4unity_WorldToObject[4];
	UNITY_UNIFORM vec4 unity_LODFade;
	UNITY_UNIFORM mediump vec4 unity_WorldTransformParams;
	UNITY_UNIFORM mediump vec4 unity_LightData;
	UNITY_UNIFORM mediump vec4 unity_LightIndices[2];
	UNITY_UNIFORM vec4 unity_ProbesOcclusion;
	UNITY_UNIFORM mediump vec4 unity_SpecCube0_HDR;
	UNITY_UNIFORM vec4 unity_LightmapST;
	UNITY_UNIFORM vec4 unity_DynamicLightmapST;
	UNITY_UNIFORM mediump vec4 unity_SHAr;
	UNITY_UNIFORM mediump vec4 unity_SHAg;
	UNITY_UNIFORM mediump vec4 unity_SHAb;
	UNITY_UNIFORM mediump vec4 unity_SHBr;
	UNITY_UNIFORM mediump vec4 unity_SHBg;
	UNITY_UNIFORM mediump vec4 unity_SHBb;
	UNITY_UNIFORM mediump vec4 unity_SHC;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
UNITY_BINDING(1) uniform UnityPerMaterial {
#endif
	UNITY_UNIFORM vec4 Color_9DAE10ED;
	UNITY_UNIFORM float Vector1_5726EE9A;
	UNITY_UNIFORM float Vector1_348DD60F;
	UNITY_UNIFORM float Vector1_DE6E0ACB;
	UNITY_UNIFORM vec4 Color_45BF8EF3;
	UNITY_UNIFORM float Vector1_65135B8E;
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
};
#endif
UNITY_LOCATION(0) uniform mediump samplerCube unity_SpecCube0;
UNITY_LOCATION(1) uniform mediump sampler2D _MainLightShadowmapTexture;
UNITY_LOCATION(2) uniform mediump sampler2DShadow hlslcc_zcmp_MainLightShadowmapTexture;
in highp vec3 vs_TEXCOORD0;
in highp vec3 vs_TEXCOORD1;
in highp vec4 vs_TEXCOORD3;
in highp vec3 vs_TEXCOORD4;
in highp vec3 vs_TEXCOORD6;
layout(location = 0) out mediump vec4 SV_TARGET0;
vec3 u_xlat0;
uint u_xlatu0;
bool u_xlatb0;
vec3 u_xlat1;
vec4 u_xlat2;
mediump vec4 u_xlat16_2;
bvec4 u_xlatb2;
vec4 u_xlat3;
mediump vec4 u_xlat16_3;
int u_xlati3;
vec3 u_xlat4;
vec3 u_xlat5;
mediump vec3 u_xlat16_6;
mediump vec3 u_xlat16_7;
mediump vec3 u_xlat16_8;
mediump vec3 u_xlat16_9;
mediump vec3 u_xlat16_10;
vec2 u_xlat11;
uint u_xlatu11;
bool u_xlatb11;
vec3 u_xlat14;
bool u_xlatb14;
float u_xlat22;
int u_xlati22;
bool u_xlatb22;
vec2 u_xlat23;
vec2 u_xlat24;
float u_xlat33;
int u_xlati33;
uint u_xlatu33;
float u_xlat34;
int u_xlati34;
uint u_xlatu34;
float u_xlat35;
mediump float u_xlat16_39;
mediump float u_xlat16_40;
void main()
{
	ImmCB_0_0_0[0] = vec4(1.0, 0.0, 0.0, 0.0);
	ImmCB_0_0_0[1] = vec4(0.0, 1.0, 0.0, 0.0);
	ImmCB_0_0_0[2] = vec4(0.0, 0.0, 1.0, 0.0);
	ImmCB_0_0_0[3] = vec4(0.0, 0.0, 0.0, 1.0);
    u_xlat0.xy = vs_TEXCOORD3.xy + vec2(-0.5, -0.5);
    u_xlat0.x = dot(u_xlat0.xy, u_xlat0.xy);
    u_xlat11.xy = vs_TEXCOORD3.yx * vec2(1.0, -1.0) + vec2(-0.5, 0.5);
    u_xlat0.xy = u_xlat11.xy * u_xlat0.xx + vs_TEXCOORD3.xy;
    u_xlat22 = _TimeParameters.x * Vector1_5726EE9A;
    u_xlat0.xy = u_xlat0.xy * vec2(vec2(Vector1_348DD60F, Vector1_348DD60F));
    u_xlat1.xy = floor(u_xlat0.xy);
    u_xlat0.xy = fract(u_xlat0.xy);
    u_xlat23.x = float(0.0);
    u_xlat23.y = float(8.0);
    for(int u_xlati_loop_1 = int(0xFFFFFFFFu) ; u_xlati_loop_1<=1 ; u_xlati_loop_1++)
    {
        u_xlat2.y = float(u_xlati_loop_1);
        u_xlat24.xy = u_xlat23.xy;
        for(int u_xlati_loop_2 = int(0xFFFFFFFFu) ; u_xlati_loop_2<=1 ; u_xlati_loop_2++)
        {
            u_xlat2.x = float(u_xlati_loop_2);
            u_xlat14.xy = u_xlat1.xy + u_xlat2.xy;
            u_xlat14.z = dot(u_xlat14.xy, vec2(15.2700005, 99.4100037));
            u_xlat14.x = dot(u_xlat14.xy, vec2(47.6300011, 89.9800034));
            u_xlat4.xy = sin(u_xlat14.xz);
            u_xlat14.xy = u_xlat4.xy * vec2(46839.3203, 46839.3203);
            u_xlat14.xy = fract(u_xlat14.xy);
            u_xlat14.xy = vec2(u_xlat22) * u_xlat14.xy;
            u_xlat14.x = sin(u_xlat14.x);
            u_xlat4.x = u_xlat14.x * 0.5 + u_xlat2.x;
            u_xlat2.x = cos(u_xlat14.y);
            u_xlat4.y = u_xlat2.x * 0.5 + u_xlat2.y;
            u_xlat14.xy = (-u_xlat0.xy) + u_xlat4.xy;
            u_xlat14.xy = u_xlat14.xy + vec2(0.5, 0.5);
            u_xlat2.x = dot(u_xlat14.xy, u_xlat14.xy);
            u_xlat2.x = sqrt(u_xlat2.x);
#ifdef UNITY_ADRENO_ES3
            u_xlatb14 = !!(u_xlat2.x<u_xlat24.y);
#else
            u_xlatb14 = u_xlat2.x<u_xlat24.y;
#endif
            u_xlat24.xy = (bool(u_xlatb14)) ? u_xlat2.xx : u_xlat24.xy;
        }
        u_xlat23.xy = u_xlat24.xy;
    }
    u_xlat0.x = log2(u_xlat23.x);
    u_xlat0.x = u_xlat0.x * Vector1_DE6E0ACB;
    u_xlat0.x = exp2(u_xlat0.x);
    u_xlat0.xyz = u_xlat0.xxx * Color_45BF8EF3.xyz + Color_9DAE10ED.xyz;
    u_xlat33 = dot(vs_TEXCOORD4.xyz, vs_TEXCOORD4.xyz);
    u_xlat33 = max(u_xlat33, 1.17549435e-38);
    u_xlat33 = inversesqrt(u_xlat33);
    u_xlat1.xyz = vec3(u_xlat33) * vs_TEXCOORD4.xyz;
    u_xlat2.xyz = vs_TEXCOORD0.xyz + (-_CascadeShadowSplitSpheres0.xyz);
    u_xlat3.xyz = vs_TEXCOORD0.xyz + (-_CascadeShadowSplitSpheres1.xyz);
    u_xlat4.xyz = vs_TEXCOORD0.xyz + (-_CascadeShadowSplitSpheres2.xyz);
    u_xlat5.xyz = vs_TEXCOORD0.xyz + (-_CascadeShadowSplitSpheres3.xyz);
    u_xlat2.x = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat2.y = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat2.z = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat2.w = dot(u_xlat5.xyz, u_xlat5.xyz);
    u_xlatb2 = lessThan(u_xlat2, _CascadeShadowSplitSphereRadii);
    u_xlat16_3.x = (u_xlatb2.x) ? float(1.0) : float(0.0);
    u_xlat16_3.y = (u_xlatb2.y) ? float(1.0) : float(0.0);
    u_xlat16_3.z = (u_xlatb2.z) ? float(1.0) : float(0.0);
    u_xlat16_3.w = (u_xlatb2.w) ? float(1.0) : float(0.0);
    u_xlat16_6.x = (u_xlatb2.x) ? float(-1.0) : float(-0.0);
    u_xlat16_6.y = (u_xlatb2.y) ? float(-1.0) : float(-0.0);
    u_xlat16_6.z = (u_xlatb2.z) ? float(-1.0) : float(-0.0);
    u_xlat16_6.xyz = u_xlat16_3.yzw + u_xlat16_6.xyz;
    u_xlat16_3.yzw = max(u_xlat16_6.xyz, vec3(0.0, 0.0, 0.0));
    u_xlat16_6.x = dot(u_xlat16_3, vec4(4.0, 3.0, 2.0, 1.0));
    u_xlat16_6.x = (-u_xlat16_6.x) + 4.0;
    u_xlatu34 = uint(u_xlat16_6.x);
    u_xlati34 = int(int(u_xlatu34) << 2);
    u_xlat2.xyz = vs_TEXCOORD0.yyy * hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati34 + 1)].xyz;
    u_xlat2.xyz = hlslcc_mtx4x4_MainLightWorldToShadow[u_xlati34].xyz * vs_TEXCOORD0.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati34 + 2)].xyz * vs_TEXCOORD0.zzz + u_xlat2.xyz;
    u_xlat2.xyz = u_xlat2.xyz + hlslcc_mtx4x4_MainLightWorldToShadow[(u_xlati34 + 3)].xyz;
    u_xlat16_6.xyz = u_xlat0.xyz * vec3(0.959999979, 0.959999979, 0.959999979);
    u_xlat0.xyz = u_xlat2.xyz + _MainLightShadowOffset0.xyz;
    vec3 txVec0 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat3.x = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec0, 0.0);
    u_xlat0.xyz = u_xlat2.xyz + _MainLightShadowOffset1.xyz;
    vec3 txVec1 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat3.y = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec1, 0.0);
    u_xlat0.xyz = u_xlat2.xyz + _MainLightShadowOffset2.xyz;
    vec3 txVec2 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat3.z = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec2, 0.0);
    u_xlat0.xyz = u_xlat2.xyz + _MainLightShadowOffset3.xyz;
    vec3 txVec3 = vec3(u_xlat0.xy,u_xlat0.z);
    u_xlat3.w = textureLod(hlslcc_zcmp_MainLightShadowmapTexture, txVec3, 0.0);
    u_xlat16_39 = dot(u_xlat3, vec4(0.25, 0.25, 0.25, 0.25));
    u_xlat16_7.x = (-_MainLightShadowParams.x) + 1.0;
    u_xlat16_39 = u_xlat16_39 * _MainLightShadowParams.x + u_xlat16_7.x;
#ifdef UNITY_ADRENO_ES3
    u_xlatb0 = !!(0.0>=u_xlat2.z);
#else
    u_xlatb0 = 0.0>=u_xlat2.z;
#endif
#ifdef UNITY_ADRENO_ES3
    u_xlatb11 = !!(u_xlat2.z>=1.0);
#else
    u_xlatb11 = u_xlat2.z>=1.0;
#endif
    u_xlatb0 = u_xlatb11 || u_xlatb0;
    u_xlat16_39 = (u_xlatb0) ? 1.0 : u_xlat16_39;
    u_xlat16_7.x = dot((-u_xlat1.xyz), vs_TEXCOORD1.xyz);
    u_xlat16_7.x = u_xlat16_7.x + u_xlat16_7.x;
    u_xlat16_7.xyz = vs_TEXCOORD1.xyz * (-u_xlat16_7.xxx) + (-u_xlat1.xyz);
    u_xlat16_40 = dot(vs_TEXCOORD1.xyz, u_xlat1.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_40 = min(max(u_xlat16_40, 0.0), 1.0);
#else
    u_xlat16_40 = clamp(u_xlat16_40, 0.0, 1.0);
#endif
    u_xlat16_40 = (-u_xlat16_40) + 1.0;
    u_xlat16_40 = u_xlat16_40 * u_xlat16_40;
    u_xlat16_40 = u_xlat16_40 * u_xlat16_40;
    u_xlat16_2 = textureLod(unity_SpecCube0, u_xlat16_7.xyz, 4.05000019);
    u_xlat16_7.x = u_xlat16_2.w + -1.0;
    u_xlat16_7.x = unity_SpecCube0_HDR.w * u_xlat16_7.x + 1.0;
    u_xlat16_7.x = max(u_xlat16_7.x, 0.0);
    u_xlat16_7.x = log2(u_xlat16_7.x);
    u_xlat16_7.x = u_xlat16_7.x * unity_SpecCube0_HDR.y;
    u_xlat16_7.x = exp2(u_xlat16_7.x);
    u_xlat16_7.x = u_xlat16_7.x * unity_SpecCube0_HDR.x;
    u_xlat16_7.xyz = u_xlat16_2.xyz * u_xlat16_7.xxx;
    u_xlat0.xyz = u_xlat16_7.xyz * vec3(0.941176474, 0.941176474, 0.941176474);
    u_xlat16_7.x = u_xlat16_40 * 0.5 + 0.0399999991;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat16_7.xxx;
    u_xlat0.xyz = vs_TEXCOORD6.xyz * u_xlat16_6.xyz + u_xlat0.xyz;
    u_xlat16_39 = u_xlat16_39 * unity_LightData.z;
    u_xlat16_7.x = dot(vs_TEXCOORD1.xyz, _MainLightPosition.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_7.x = min(max(u_xlat16_7.x, 0.0), 1.0);
#else
    u_xlat16_7.x = clamp(u_xlat16_7.x, 0.0, 1.0);
#endif
    u_xlat16_39 = u_xlat16_39 * u_xlat16_7.x;
    u_xlat16_7.xyz = vec3(u_xlat16_39) * _MainLightColor.xyz;
    u_xlat2.xyz = vs_TEXCOORD4.xyz * vec3(u_xlat33) + _MainLightPosition.xyz;
    u_xlat33 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat33 = max(u_xlat33, 1.17549435e-38);
    u_xlat33 = inversesqrt(u_xlat33);
    u_xlat2.xyz = vec3(u_xlat33) * u_xlat2.xyz;
    u_xlat33 = dot(vs_TEXCOORD1.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat33 = min(max(u_xlat33, 0.0), 1.0);
#else
    u_xlat33 = clamp(u_xlat33, 0.0, 1.0);
#endif
    u_xlat34 = dot(_MainLightPosition.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
    u_xlat34 = min(max(u_xlat34, 0.0), 1.0);
#else
    u_xlat34 = clamp(u_xlat34, 0.0, 1.0);
#endif
    u_xlat33 = u_xlat33 * u_xlat33;
    u_xlat33 = u_xlat33 * -0.9375 + 1.00001001;
    u_xlat16_39 = u_xlat34 * u_xlat34;
    u_xlat33 = u_xlat33 * u_xlat33;
    u_xlat34 = max(u_xlat16_39, 0.100000001);
    u_xlat33 = u_xlat33 * u_xlat34;
    u_xlat33 = u_xlat33 * 3.0;
    u_xlat33 = 0.0625 / u_xlat33;
    u_xlat16_39 = u_xlat33 + -6.10351563e-05;
    u_xlat16_8.xyz = vec3(u_xlat16_39) * vec3(0.0399999991, 0.0399999991, 0.0399999991) + u_xlat16_6.xyz;
    u_xlat16_7.xyz = u_xlat16_8.xyz * u_xlat16_7.xyz + u_xlat0.xyz;
    u_xlat16_39 = min(_AdditionalLightsCount.x, unity_LightData.y);
    u_xlatu0 = uint(int(u_xlat16_39));
    u_xlat16_8.xyz = u_xlat16_7.xyz;
    for(uint u_xlatu_loop_3 = uint(0u) ; u_xlatu_loop_3<u_xlatu0 ; u_xlatu_loop_3++)
    {
        u_xlati22 = int(uint(u_xlatu_loop_3 & 3u));
        u_xlatu33 = uint(u_xlatu_loop_3 >> 2u);
        u_xlat16_39 = dot(unity_LightIndices[int(u_xlatu33)], ImmCB_0_0_0[u_xlati22]);
        u_xlati22 = int(u_xlat16_39);
        u_xlat2.xyz = (-vs_TEXCOORD0.xyz) * _AdditionalLightsPosition[u_xlati22].www + _AdditionalLightsPosition[u_xlati22].xyz;
        u_xlat33 = dot(u_xlat2.xyz, u_xlat2.xyz);
        u_xlat33 = max(u_xlat33, 6.10351563e-05);
        u_xlat34 = inversesqrt(u_xlat33);
        u_xlat4.xyz = vec3(u_xlat34) * u_xlat2.xyz;
        u_xlat35 = float(1.0) / float(u_xlat33);
        u_xlat33 = u_xlat33 * _AdditionalLightsAttenuation[u_xlati22].x + _AdditionalLightsAttenuation[u_xlati22].y;
#ifdef UNITY_ADRENO_ES3
        u_xlat33 = min(max(u_xlat33, 0.0), 1.0);
#else
        u_xlat33 = clamp(u_xlat33, 0.0, 1.0);
#endif
        u_xlat33 = u_xlat33 * u_xlat35;
        u_xlat16_39 = dot(_AdditionalLightsSpotDir[u_xlati22].xyz, u_xlat4.xyz);
        u_xlat16_39 = u_xlat16_39 * _AdditionalLightsAttenuation[u_xlati22].z + _AdditionalLightsAttenuation[u_xlati22].w;
#ifdef UNITY_ADRENO_ES3
        u_xlat16_39 = min(max(u_xlat16_39, 0.0), 1.0);
#else
        u_xlat16_39 = clamp(u_xlat16_39, 0.0, 1.0);
#endif
        u_xlat16_39 = u_xlat16_39 * u_xlat16_39;
        u_xlat33 = u_xlat33 * u_xlat16_39;
        u_xlat16_39 = dot(vs_TEXCOORD1.xyz, u_xlat4.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat16_39 = min(max(u_xlat16_39, 0.0), 1.0);
#else
        u_xlat16_39 = clamp(u_xlat16_39, 0.0, 1.0);
#endif
        u_xlat16_39 = u_xlat33 * u_xlat16_39;
        u_xlat16_9.xyz = vec3(u_xlat16_39) * _AdditionalLightsColor[u_xlati22].xyz;
        u_xlat2.xyz = u_xlat2.xyz * vec3(u_xlat34) + u_xlat1.xyz;
        u_xlat22 = dot(u_xlat2.xyz, u_xlat2.xyz);
        u_xlat22 = max(u_xlat22, 1.17549435e-38);
        u_xlat22 = inversesqrt(u_xlat22);
        u_xlat2.xyz = vec3(u_xlat22) * u_xlat2.xyz;
        u_xlat22 = dot(vs_TEXCOORD1.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat22 = min(max(u_xlat22, 0.0), 1.0);
#else
        u_xlat22 = clamp(u_xlat22, 0.0, 1.0);
#endif
        u_xlat33 = dot(u_xlat4.xyz, u_xlat2.xyz);
#ifdef UNITY_ADRENO_ES3
        u_xlat33 = min(max(u_xlat33, 0.0), 1.0);
#else
        u_xlat33 = clamp(u_xlat33, 0.0, 1.0);
#endif
        u_xlat22 = u_xlat22 * u_xlat22;
        u_xlat22 = u_xlat22 * -0.9375 + 1.00001001;
        u_xlat16_39 = u_xlat33 * u_xlat33;
        u_xlat22 = u_xlat22 * u_xlat22;
        u_xlat33 = max(u_xlat16_39, 0.100000001);
        u_xlat22 = u_xlat33 * u_xlat22;
        u_xlat22 = u_xlat22 * 3.0;
        u_xlat22 = 0.0625 / u_xlat22;
        u_xlat16_39 = u_xlat22 + -6.10351563e-05;
        u_xlat16_10.xyz = vec3(u_xlat16_39) * vec3(0.0399999991, 0.0399999991, 0.0399999991) + u_xlat16_6.xyz;
        u_xlat16_8.xyz = u_xlat16_10.xyz * u_xlat16_9.xyz + u_xlat16_8.xyz;
    }
    SV_TARGET0.xyz = u_xlat16_8.xyz;
    SV_TARGET0.w = 1.0;
    return;
}

#endif
                               $Globals 
        _MainLightPosition                           _MainLightColor                         _AdditionalLightsCount                           _AdditionalLightsPosition                     0      _AdditionalLightsColor                    0     _AdditionalLightsAttenuation                  0     _AdditionalLightsSpotDir                  0     _TimeParameters                   0     _CascadeShadowSplitSpheres0                   �	     _CascadeShadowSplitSpheres1                   �	     _CascadeShadowSplitSpheres2                   �	     _CascadeShadowSplitSpheres3                   �	     _CascadeShadowSplitSphereRadii                    �	     _MainLightShadowOffset0                   �	     _MainLightShadowOffset1                   �	     _MainLightShadowOffset2                   �	     _MainLightShadowOffset3                    
     _MainLightShadowParams                    
     _MainLightWorldToShadow                 @         UnityPerDraw�        unity_LODFade                     �      unity_WorldTransformParams                    �      unity_LightData                   �      unity_LightIndices                   �      unity_ProbesOcclusion                     �      unity_SpecCube0_HDR                   �      unity_LightmapST                  �      unity_DynamicLightmapST                      
   unity_SHAr                      
   unity_SHAg                       
   unity_SHAb                    0  
   unity_SHBr                    @  
   unity_SHBg                    P  
   unity_SHBb                    `  	   unity_SHC                     p     unity_ObjectToWorld                         unity_WorldToObject                  @          UnityPerMaterial4         Color_9DAE10ED                           Vector1_5726EE9A                        Vector1_348DD60F                        Vector1_DE6E0ACB                        Color_45BF8EF3                           Vector1_65135B8E                  0          $Globals�  	      _TimeParameters                          _WorldSpaceCameraPos                        _CascadeShadowSplitSpheres0                   �     _CascadeShadowSplitSpheres1                   �     _CascadeShadowSplitSpheres2                   �     _CascadeShadowSplitSpheres3                   �     _CascadeShadowSplitSphereRadii                    �     unity_MatrixVP                          _MainLightWorldToShadow                 `             unity_SpecCube0                   _MainLightShadowmapTexture                  UnityPerDraw              UnityPerMaterial          